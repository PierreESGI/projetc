#ifndef ACCOUNT_H
#define ACCOUNT_H
int createAccount();
int consultAccount();
int displayAccount(int clientID);
int deleteAccount(void);
int getLastIDFromAccountFile();
int depotAccount();
int withdrawAccount();

 
#endif