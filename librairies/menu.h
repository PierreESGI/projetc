#ifndef MENU_H
#define MENU_H

int get_menu_choice(void);
int get_menu_client(void);
int get_menu_compte(void);
int get_menu_administration(void);
#endif
