#ifndef USER_H
#define USER_H

int userCreate(void);
int userDelete(void);
int userEdit(void);
int DisplayAccounts(void);
int getLastIDFromUserFile(void);
#endif