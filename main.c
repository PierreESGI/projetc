#include <stdio.h>
#include "./librairies/menu.h"
#include "./librairies/account.h"
#include "./librairies/user.h"
#include "./librairies/auth.h"
#include "./librairies/log.h"
#include "./librairies/admin.h"

int main( int argc, char **argv ){
     int choice;
     int result;
    choice = get_menu_choice();
    //Charge le contenu du csv dans des structures (Struct user and struct account )
    //Retrourne les deux structures dans deux fonctions differentes (defUser() and defAccount())
    switch (choice)
    {
        case 1:
            result = get_menu_client();
            switch (result)
            {
                case 1:
                    userCreate();
                    break;
                case 2:
                    userDelete();
                    break;
                case 3:
                    userEdit();
                    break;
                case 4:
                    DisplayAccounts();
                    break;
                default:
                    get_menu_client();
                    break;
            }
            break;
        case 2:
            result = get_menu_compte();
            switch (result)
            {
                case 1:
                    createAccount();
                    break;
                case 2:
                    deleteAccount();
                    break;
                case 3:
                    userEdit();
                    break;
                case 4:
                    consultAccount();
                    break;
                case 5:
                    withdrawAccount();
                    break;
                case 6:
                    depotAccount();
                    break;
                default:
                    get_menu_compte();
                    break;
            }
            break;
        case 3:
            result = get_menu_administration();
	    printf("%d",result);
	    switch (result)
	    {
		case 1:
		    sommeCompte();
	    	    break;
		case 2:
		    sommeTypeCompte();
		    break;
		case 3:
		    exportData();
		    break;
		case 4:
		    importData();
		    break;
		case 999:
		    break;
		default:
		    get_menu_administration();
		    break;
	    }
            break;
    }
    return 0;

}
