Features :

-Ajouter un client :
	-Ajout automatique d un identifiant lors de la creation de l utilisateur OK
	-Entree de valeur dans users.csv OK

-Modification du client :
	-Lire les valeur presentes dans users.csv OK
	-Modifier les valeurs presentes dans account.csv

-Supprimer un client: 
	-Supprimer une ligne d un fichier depuis un identifiant unique
	-Supression de fichier ou valeurs dans un autres fichier (compte ?)

-Recherche du client:
	-Rechercher dans un cvs un match regex

-Compte :
	-Affichage compte utilisateur
	-Affichage info compte particulier de l utilisateur 

-Action sur le compte:
	-Depots prennant en compte le % du taux
	-Retrait Avec verif du compte 
	-Virement d argent d un compte a un autre

-Logging
	-Mise en place d un fichier de log d au moins une semaine OK

-Administration:
	-Mise en place d un mot de passe pour administration 	OK
	-Addition de numeros dans un fichier csv
	-Affichage des montants par type de compte
	-Exporter donnes dans l application
	-Importation des donnes 

Info :

-Valeur d'un user :
	-Identifiant(ID),proprietaire (nom,prenom)

-Valeur d un compte :
	-id du compte, numero d identifiant user associe, solde, taux, duree avant mise a dispo des fonds



-Creation des menu OK
-Ajout des valeurs dans le fichier OK
-Lecture des valeurs depuis le fichier OK
-Modification des valeurs dans le fichier
-Addition des valeurs dans le fichier
-Creation de l authentification OK
