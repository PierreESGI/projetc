#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../librairies/menu.h"
#include "../librairies/log.h"
#include <time.h>

int getLastIDFromAccountFile(){
    char buf[1024];
	FILE *fp = fopen("./data/account.csv", "r");
	if (!fp) {
		printf("Can't open file\n");
		writeToLogFile("ERREUR","Can't open file users.csv at getLastIDFromUserFile");
		return 1;
	}
	while (fgets(buf, 1024, fp)){
	}
	//printf("%d",atoi(strtok(buf, ";")));
	strtok(buf, ";");
	int res = atoi(strtok(NULL, ";"));
	//printf("Res : %d\n",res);
	fclose(fp);
	return res;	
}

int createAccount(void){
	int clientID;
	char soldeCompte[40];
	char *tauxCompte;
	int dureefonds;
	int idCompte = getLastIDFromAccountFile() +1;
	printf(" IDCOMPTE : %d\n",idCompte);
	char *choice = malloc(1);
	char buf[1024];
	int idTypeCompte;
	char *typeCompteBackup;
	char *nomTypeCompte;
	time_t creationTime;


	//timestamp shenanigans

	time(&creationTime);


	tauxCompte = malloc(100);
	FILE *accountFile = fopen("./data/account.csv","a");
	FILE *typeCompte = fopen("./data/typecompte.csv","r");

	//ID;IDCOMPTE;SOLDECOMPTE;TAUXCOMPTE;DUREEFONDS structure fichier compte
	if(accountFile != NULL){
		printf("Entrez le numéro du client pour qui le compte est créé :\n");
		scanf("%d", &clientID );

		printf("Entrez le solde du nouveau compte :\n");
        fprintf(stdin, "a\n");
        fflush(stdin);
        scanf("%s", &soldeCompte );

        printf("|%s|",soldeCompte);

		printf("Voulez vous choisir un type de compte (y/n) ?");
		scanf("%s",choice);
		if(strcmp("y", choice) == 0){
			printf("Tapez l'id du type de compte que vous souhaitez creér : \n");
			scanf("%d",&idTypeCompte);
			if(accountFile != NULL){
				while (fgets(buf, 1024, typeCompte))
				{
					buf[strlen(buf) - 1] = '\0'; //strip new line
					typeCompteBackup = malloc(1+ strlen(buf));
					if (typeCompteBackup){
						strcpy(typeCompteBackup,buf);// backup de la ligne extraite
					}else{
						printf("error malloc");
						writeToLogFile("ERREUR","no malloc possible");
					}
					if(idTypeCompte == atoi(strtok(buf,";"))){
						nomTypeCompte = strtok(NULL,";");
						dureefonds = atoi(strtok(NULL,";"));
						tauxCompte = strtok(NULL,";");
						fprintf(accountFile,"%d;%d;%ld;%s;%s;%d;%s\n",clientID,idCompte,creationTime,soldeCompte,tauxCompte,dureefonds,nomTypeCompte);
						//free(tauxCompte);
						free(typeCompteBackup);
					}					
				}
			}
		}else if (strcmp("n",choice) == 0){

			printf("Entrez le taux associé à ce compte (1.45 pour 45 pourcent) :\n");
			scanf("%s", tauxCompte );
			printf("Inserez la duree de blocage des fonds en jours :\n");
			scanf("%d", &dureefonds );
			fprintf(accountFile,"%d;%d;%ld;%s;%s;%d\n",clientID,idCompte,creationTime,soldeCompte,tauxCompte,dureefonds);
			//free(tauxCompte);
		}
		
        fclose(accountFile); //Libere l'acces au fichier 
		fclose(typeCompte);
		writeToLogFile("INFO","Nouveau compte crée !");

	}else{
		writeToLogFile("ERREUR","can't open account file at createAccount");
	}	
	return 0;
}

int consultAccount(void){
	int idUser;
	int idAccount;
	int chosenAccount;
	int chosenUser;
	char choice[1] = "\0";
	char buf[1024];
	char *bufBackup;
	FILE *accountFile = fopen("./data/account.csv", "r");
	printf("Entrez l'id de l'utilisateur pour lequel vous souhaitez voir les comptes");
	scanf("%d",&idUser);
	printf("Souhaiter vous voir tout les comptes ? (y/n): \n");
	scanf("%s",choice);
	if (strcmp(choice,"y") == 0 )
	{
		printf("IDCLIENT;IDCOMPTE;SOLDE;TAUX;DUREE;TYPECOMPTE \n");
		if(accountFile != NULL){
			while (fgets(buf, 1024, accountFile))
			{	
				bufBackup = malloc(strlen(buf)+1);
				if (bufBackup)
				{
					strcpy(bufBackup,buf);
				}else
				{
					writeToLogFile("ERREUR","no malloc possible");
				}
				
				
				if (atoi(strtok(buf,";")) == idUser)
				{
					printf("%s",bufBackup);
				}
				
			}
			
		}
	}else{
	    printf("Entrez l'id du compte que vous souhiatez consultez : \n");
	    scanf("%d",&idAccount);
	    if(accountFile != NULL){
	        while (fgets(buf, 1024, accountFile)){

                bufBackup = malloc(strlen(buf)+1);
                if (bufBackup)
                {
                    strcpy(bufBackup,buf);
                }

                chosenUser = atoi(strtok(buf,";"));
                chosenAccount = atoi(strtok(NULL,";"));
                    if(idAccount == chosenAccount && idUser == chosenUser){
                        printf("IDCLIENT;IDCOMPTE;SOLDE;TAUX;DUREE;TYPECOMPTE \n");
                        printf("%s", bufBackup);
                    }
	        }
	    }
	}
	

}

int deleteAccount(void){

    int idCompte;
    char buf[1024];
    char *bufBackup;
    char *idAccount;


    //fichiers
    FILE *oldAccounts = fopen("./data/account.csv","r");
    FILE *newAccounts = fopen("./data/newAccount.csv","w");



    printf("Entrez l' id du compte que vous souhaitez supprimer \n");
    scanf("%d",&idCompte); //id tapé
    if(oldAccounts != NULL) {

        while (fgets(buf, 1024, oldAccounts)) {

            bufBackup = malloc(strlen(buf) + 1);
            if(bufBackup){
                strcpy(bufBackup,buf);
            } else {
                printf("No malloc possible");
            }

            strtok(buf,";");
            idAccount = atoi(strtok(NULL,";")); //id extrait du fichier
            if(newAccounts && idAccount != idCompte){
                fputs(bufBackup, newAccounts);
            }
        }

        fclose(oldAccounts);
        fclose(newAccounts);
        free(bufBackup);


        int status = remove("./data/account.csv");

        if (status == 0){
            printf("account.csv file deleted successfully.\n");
            writeToLogFile("INFO","old account file deleted");
        }else{
            writeToLogFile("ERREUR","can't delete old account file");
            printf("Unable to delete the file\n");
        }

        rename("./data/newAccount.csv","./data/account.csv");
    }
    return 0;
}

int depotAccount(){
    char buf[1024];
    char *bufBackup;
    float amount;
    int idCompte;
    int idCompteFromFile;
    int idClientFromFile;
    char *timestampCrea;
    int  dureefonds;
    char *typeCompteFromFile;
    float soldeCompteFromFile;
    float tauxCompteFromFile;

    float result;

    FILE *oldAccount = fopen("./data/account.csv","r");
    FILE *newAccount = fopen("./data/newAccount.csv","w");

    printf("Veuillez entrer l'id du compte sur lequel vous voulez faire un dépot : \n");
    scanf("%d",&idCompte);

    if(oldAccount != NULL) {

        while (fgets(buf, 1024, oldAccount)) {

            //save buf
            bufBackup = malloc(strlen(buf) + 1);
            if (bufBackup){
                strcpy(bufBackup,buf);
            } else {
                printf("no malloc possible");
                writeToLogFile("ERREUR","Pas de Malloc possible at depotcompte");
            }


            idClientFromFile = atoi(strtok(buf, ";"));//ID
            idCompteFromFile = atoi(strtok(NULL, ";"));//IDCOMPTE
            timestampCrea =  strtok(NULL, ";");//timestampCréa
            soldeCompteFromFile = atoi(strtok(NULL, ";"));//SoldeCompte
            tauxCompteFromFile = atof(strtok(NULL, ";"));//tauxCompte
            dureefonds = atoi(strtok(NULL, ";"));//Dureefonds
            typeCompteFromFile = strtok(NULL, ";");//TypeCompte

            if(newAccount != NULL){
                if(idCompte == idCompteFromFile){

                    printf("Veuillez entré le montant déposé : \n");
                    scanf("%f",&amount);
                    result = (soldeCompteFromFile + amount) * tauxCompteFromFile;
                    fprintf(newAccount,"%d;%d;%s;%f;%f;%d\n",idClientFromFile,idCompte,timestampCrea,result,tauxCompteFromFile,dureefonds);
                    recordTransaction("Depot",idCompte,amount,1);
                } else {
                    fputs(bufBackup, newAccount);
                }
            }else{
                writeToLogFile("ERREUR","Could not create new accountFile at depotAccount");
            }
        }

        fclose(oldAccount);
        fclose(newAccount);
        //free(bufBackup);

        int status = remove("./data/account.csv");

        if (status == 0){
            printf("account.csv file deleted successfully.\n");
            writeToLogFile("INFO","old account file deleted");
        }else{
            writeToLogFile("ERREUR","can't delete old account file");
            printf("Unable to delete the file\n");
        }

        rename("./data/newAccount.csv","./data/account.csv");



    } else {
        writeToLogFile("ERREUR","Could not open account.csv file at depotAccount");
    }
    return 0;
}

int withdrawAccount(){
    char buf[1024];
    char *bufBackup;
    float amount;
    int idCompte;
    int idCompteFromFile;
    int idClientFromFile;
    int timestampCrea;
    int  dureefonds;
    char *typeCompteFromFile;
    float soldeCompteFromFile;
    float tauxCompteFromFile;
    int joursSec = 86400;
    float result;
    time_t withdrawTime;


    //timestamp shenanigans

    time(&withdrawTime);


    FILE *oldAccount = fopen("./data/account.csv","r");
    FILE *newAccount = fopen("./data/newAccount.csv","w");

    printf("Veuillez entrer l'id du compte sur lequel vous voulez faire un retrait : \n");
    scanf("%d",&idCompte);
    printf("Veuillez entrer la somme a retirer du compte : \n");
    scanf("%f",&amount);

    if(oldAccount != NULL){

        while (fgets(buf, 1024, oldAccount)){

            //save buf
            bufBackup = malloc(strlen(buf) + 1);
            if (bufBackup){
                strcpy(bufBackup,buf);
            } else {
                printf("no malloc possible");
                writeToLogFile("ERREUR","Pas de Malloc possible at depotcompte");
            }

            idClientFromFile = atoi(strtok(buf, ";"));//ID
            idCompteFromFile = atoi(strtok(NULL, ";"));//IDCOMPTE
            timestampCrea =  atoi(strtok(NULL, ";"));//timestampCréa
            soldeCompteFromFile = atoi(strtok(NULL, ";"));//SoldeCompte
            tauxCompteFromFile = atof(strtok(NULL, ";"));//tauxCompte
            dureefonds = atoi(strtok(NULL, ";"));//Dureefonds
            typeCompteFromFile = strtok(NULL, ";");//TypeCompte

            if (newAccount != NULL){
                if(idCompte == idCompteFromFile){
                    if(timestampCrea + (dureefonds * joursSec) <= (long double)withdrawTime){
                        if((soldeCompteFromFile - amount) >= 0.0){
                            result = soldeCompteFromFile - amount;
                            fprintf(newAccount,"%d;%d;%d;%f;%f;%d;%s",idClientFromFile, idCompteFromFile, timestampCrea, result, tauxCompteFromFile, dureefonds, typeCompteFromFile);
                            recordTransaction("Retrait",idCompte,amount,1);
                        } else {
                            printf("vous n'avez pas les fonds nécessaire pour cette action");
                            recordTransaction("Retrait" ,idCompte, amount, 0);
                            fputs(bufBackup,newAccount);
                        }
                    } else {
                        printf("votre compte est encore vérouillé");
                        recordTransaction("Retrait",idCompte,amount,0);
                        fputs(bufBackup,newAccount);
                    }
                } else {
                    fputs(bufBackup,newAccount);
                }
            }
        }

        fclose(oldAccount);
        fclose(newAccount);
        free(bufBackup);


        int status = remove("./data/account.csv");

        if (status == 0){
            printf("account.csv file deleted successfully.\n");
            writeToLogFile("INFO","old account file deleted");
        }else{
            writeToLogFile("ERREUR","can't delete old account file");
            printf("Unable to delete the file\n");
        }

        rename("./data/newAccount.csv","./data/account.csv");

    } else {
        writeToLogFile("ERREUR","Could not open account.csv file at depotAccount");
    }
    return 0;
}