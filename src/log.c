#include "../librairies/log.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

void writeToLogFile(char *logType, char *message){
    FILE* f = fopen("./logs/logs.txt","a+");
    if (f == NULL)
    {
        printf("Le fichier ne peut être ouvert");
        exit(1);
    }

    time_t timestamp;
    timestamp = time(NULL);
    char *finaltime = ctime(&timestamp);
    finaltime[strlen(finaltime)-1]='\0'; //permet d'enlever le retour a la ligne sur le timestamp

    fprintf(f,"[%s]%s : %s\n",logType, finaltime , message);
    fclose(f);
    
}

void recordTransaction(char *actionName, int idCompte, float montant, int successful){
    FILE *transactionHistory = fopen("./logs/transactionHistory.txt","a+");
    if(transactionHistory != NULL){
        time_t timestamp;
        timestamp = time(NULL);
        char *finaltime = ctime(&timestamp);
        finaltime[strlen(finaltime)-1]='\0'; //permet d'enlever le retour a la ligne sur le timestamp
        if(successful == 1) {
            fprintf(transactionHistory, "[%s] %s : %s  depuis le compte %d pour %f \n", actionName, finaltime, actionName,
                    idCompte, montant);
        } else{
            fprintf(transactionHistory,"[ %s ] %s : Echec %s depuis le compte %d pour %f \n",actionName,finaltime,actionName, idCompte, montant);
        }
    }
}