#include <stdio.h>
#include <stdlib.h>
#include "../librairies/auth.h"
#include "../librairies/log.h"
#include "../librairies/account.h"
//Affiche le menu principal de selection des valeurs
int get_menu_choice(void)
{
    int selection = 0;
    do{
        system("clear");
        printf("\n1 - Gestion du client");
        printf("\n2 - Administration des comptes");
        printf("\n3 - Administration de la banque");
        printf("\nChoissez votre action : ");
        scanf("%d", &selection );
    } while ((selection < 1) || (selection > 3));
        return selection;
}

int get_menu_client(void)
{
    int selection = 0;
    do{
        system("clear");
        printf("\n1-Ajouter un utilisateur");
        printf("\n2-Supprimer des utilisateurs");
        printf("\n3-Modifier un utilisateur");
        printf("\n4-Afficher la liste des utilisateurs");
        printf("\nChoissez votre action : ");
        scanf("%d", &selection );
    }while ((selection < 1) || (selection > 4));
        return selection;
}

int get_menu_compte(void)
{
    int selection = 0;
    do
    {
    system("clear");
      printf("\n1-Creer un compte");
      printf("\n2-Supprimer un compte");
      printf("\n3-Modifier un compte");
      printf("\n4-Consulter un compte");
      printf("\n5-Faire un retrait d' un compte");
      printf("\n6-Faire un dépôt sur un compte");
      printf("\nChoissez votre action : ");
      scanf("%d", &selection);
    } while ((selection < 1) || (selection > 6));
    return selection;
}

int get_menu_administration(void)
{
    int login = 2;
    login = get_password();
    if (login == 0) {
        writeToLogFile("INFO","Successful connexion");
        get_menu_compte();
    }
    else
    {
        printf("ERREUR DE CONNEXION");
        writeToLogFile("ERREUR", "ERREUR DE CONNEXION");
        // LOG erreur de connexion

    }
}
 
