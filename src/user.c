#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../librairies/menu.h"
#include "../librairies/log.h"

int getLastIDFromUserFile(){
	FILE *fp = fopen("./data/users.csv", "r");
	char buf[1024];
	if (!fp) {
		printf("Can't open file\n");
		writeToLogFile("ERREUR","Can't open file users.csv at getLastIDFromUserFile");
		return 1;
	}
	while (fgets(buf, 1024, fp) != NULL)
	{
	}
	fclose(fp);
	//printf("%d",atoi(strtok(buf, ";")));
	return atoi(strtok(buf, ";"));		
}


int DisplayAccounts(void) {
	FILE *fp = fopen("./data/users.csv", "r");
	char buf[1024];
    int row_count = 0;
	int field_count = 0;

	if (!fp) {
		printf("Can't open file\n");
		writeToLogFile("ERREUR","Can't open file users.csv at DisplayAccount");
		return 1;
	}

	while (fgets(buf, 1024, fp)) {
		field_count = 0;
		row_count++;

		if (row_count == 1) {
			continue;
		}

		char *field = strtok(buf, ";");
		while (field) {
			if (field_count == 0) {
				printf("ID:\t");
			}
			if (field_count == 1) {
				printf("Last Name:\t");
			}
			if (field_count == 2) {
					printf("First Name:\t");
			}
			if (field_count == 3) {
				printf("Solde:\t");
			}
            if (field_count == 4) {
                printf("Account type:\t");
            }

			printf("%s\n", field);
			field = strtok(NULL, ";");

            

			field_count++;
		}
		printf("\n");
        printf("-----------------------------------------------------\n");
	}

	fclose(fp);
    printf("Appuyez sur entree pour retourner au menu de selection");
    fflush(stdin);
    getchar();
    while(getchar()!='\n');
    //printf("\n"); 
    get_menu_client(); 
    //return 0;
}

int userCreate(void){
	getLastIDFromUserFile();
    FILE *userptr;
    userptr = fopen("./data/users.csv","a");
    if(userptr != NULL){
        //ID;NOM;PRENOM;SOLDE;TYPECOMPTE <-- architecture CVS file 
        int idNumber = getLastIDFromUserFile() + 1;
        float solde;
        char nom[40],prenom[40],profession[20],num[15];
        printf("Tapez le nom du nouveau client :\n");
        scanf("%s", nom );
        printf("Tapez le prenom du nouveau client :\n");
        scanf("%s", prenom );
        printf("Inserez la profession du nouvveau client :\n");
        scanf("%s", profession );
		printf("Tapez le numéro de téléphone du client");
		scanf("%s",num);
        fprintf(userptr,"%d;%s;%s;%s;%s\n",idNumber,nom,prenom,profession,num);
        fclose(userptr); //Libere l'acces au fichier 

    }
    else
    {
        printf("ERREUR D OUVERTURE DE FICHIER");
		writeToLogFile("ERREUR", "Can't open users.csv at UserCreate");
        //ERREUR
    }
	return 0;
}

int userDelete(void){
	int clientID;
	int clientIDFromFile;
	char *nom;
	char *prenom;
	char *profession;
	char *num;
	char buf[1024];
	char bufAccount[1024];
	char *bufAccountBackup;

	FILE *newuserFile;
	newuserFile = fopen("./data/newUsers.csv","w");
	char *user;

	FILE *accountFile = fopen("./data/account.csv","r");
	FILE *newAccount = fopen("./data/newAccount.csv","w");


	printf("Saisissez l'ID du client que vous souhaitez supprimer : \n");
	scanf("%d",&clientID);
    //Fonction de suppression d'un utilisateur
	FILE *userptr;
    userptr = fopen("./data/users.csv","r");
    if(userptr != NULL){
        //ID;NOM;PRENOM;profession;num <-- architecture CVS file 
        while (fgets(buf, 1024, userptr))
		{
			user = malloc(1+ strlen(buf));
			if (user){
				strcpy(user,buf);// backup de la ligne extraite
			}else{
				printf("error malloc");
				writeToLogFile("ERREUR","no malloc possible");
			}
			
			
			
			if (clientID != atoi(strtok(buf, ";")))//extraxion id client + comparaison
			{
				if(newuserFile != NULL){
					//printf("%s", user);
					fputs(user,newuserFile); //constitution du nouveau fichier sans l'utilisateur supprimé
				}
				if(accountFile != NULL && newAccount != NULL){
				    while (fgets(bufAccount, 1024, accountFile)){

                        bufAccountBackup = malloc(strlen(bufAccount) +1);
                        if(bufAccountBackup){
                            strcpy(bufAccountBackup,bufAccount);
                        } else {
                            printf("no malloc possible");
                            writeToLogFile("ERREUR","no malloc possible at deleteUser");
                        }

				        clientIDFromFile = atoi(strtok(bufAccount,";"));
				        if(clientIDFromFile != clientID){
                            fputs(bufAccountBackup,newAccount);
				        }
				    }
				}else{
				    writeToLogFile("ERREUR","Could not open account files");
				}
				
			}
		}
		fclose(userptr);
		fclose(newuserFile);
		fclose(accountFile);
		fclose(newAccount);
		//free(user);
		//free(bufAccountBackup);

		int status = remove("./data/users.csv"); //effacement ancien fichier
		int statusAccount = remove("./data/account.csv");

        if (statusAccount == 0){
            printf("account.csv file deleted successfully.\n");
            writeToLogFile("INFO","ols account file deleted");
        }else{
            writeToLogFile("ERREUR","can't delete old account file");
            printf("Unable to delete the file\n");
        }
 
		if (status == 0){
			printf("users.csv file deleted successfully.\n");
			writeToLogFile("INFO","ols user file deleted");
		}else{
			writeToLogFile("ERREUR","can't delete old user file");
			printf("Unable to delete the file\n");
		}

		rename("./data/newUsers.csv","./data/users.csv"); // renommage nouveau fichier
		rename("./data/newAccount.csv","./data/account.csv"); // renomage du fichier
 
		
    }else{
        printf("ERREUR D OUVERTURE DE FICHIER");
		writeToLogFile("ERREUR", "Can't open users.csv at UserCreate");
        //ERREUR
    }
    printf("Supprime des utilisateurs\n");
	writeToLogFile("INFO","a user was deleted");
	return 0;
}

int userEdit(void){
    //ID;NOM;PRENOM;SOLDE;TYPECOMPTE <-- architecture CVS file 
    //Fonction permettant la modification d un utilisateur

	int clientID;
	char *clientIDFromFile;
	char *nom;
	char *prenom;
	char *profession;
	char *num;
	char buf[1024];
	char choice[1];

	char *newNom;
	char *newPrenom;
	char *newProfession;
	char *newNum;

	FILE *newuserFile;
	newuserFile = fopen("./data/modUsers.csv","w");
	char *user;


	printf("Saisissez l'ID du client que vous souhaitez modifier : \n");
	scanf("%d",&clientID);
    //Fonction de suppression d'un utilisateur
	FILE *userptr;
    userptr = fopen("./data/users.csv","r");
    if(userptr != NULL){
        //ID;NOM;PRENOM;SOLDE;TYPECOMPTE <-- architecture CVS file 
        while (fgets(buf, 1024, userptr))
		{
			user = malloc(1+ strlen(buf));
			if (user){
				strcpy(user,buf);// backup de la ligne extraite
			}else{
				printf("error malloc");
				writeToLogFile("ERREUR","no malloc possible");
			}
			
			
			
			if (clientID == atoi(strtok(buf, ";")))//extraxion id client + comparaison
			{
				nom = strtok(NULL,";");
				prenom = strtok(NULL,";");
				profession = strtok(NULL,";");
				num = strtok(NULL,";");

				printf("Voulez vous modifier le nom du client ? (y/n) :\n");
				scanf("%s",choice);
				newNom = (char *)malloc(100);
				if(strcmp(choice,"y")==0){
				printf("%s", choice);
				printf("Saisisez le nouveau nom du client : (%s)",nom);
				scanf("%s",newNom);
				}else{
					newNom = (char *)malloc(100);
					newNom = nom;
				}
				

				printf("Voulez vous modifier le prénom du client ? (y/n) :\n");
				scanf("%s",choice);
				if(strcmp(choice,"y") == 0){

				printf("Saisissez le nouveau prénom du client : (%s) \n",prenom);
				newPrenom = (char *)malloc(100);
				scanf("%s", newPrenom);
				}else{
					newPrenom = (char *)malloc(100);
					newPrenom = prenom;
				}

				printf("Voulez vous modifier la profession du client ? (y/n) :\n");
				scanf("%s",choice);
				if(strcmp(choice,"y") == 0){

				printf("Saisissez la nouvelle profession du client : (%s) \n",profession);
				newProfession = (char *)malloc(100);
				scanf("%s", newProfession);
				}else{
					newProfession = (char *)malloc(100);
					newProfession = prenom;
				}

				printf("Voulez vous modifier le téléphone du client ? (y/n) :\n");
				scanf("%s",choice);
				if(strcmp(choice,"y") == 0){

				printf("Saisissez la nouvelle profession du client : (%s) \n",num);
				newNum = (char *)malloc(100);
				scanf("%s", newNum);
				}else{
					newNum = (char *)malloc(100);
					newNum = num;
				}
				//printf("nosegfault");
				
				if(newuserFile != NULL){

				fprintf(newuserFile,"%d;%s;%s;%s;%s",clientID,newNom,newPrenom,newProfession,newNum);
				free(newPrenom);
				free(newNom);
				}
				
			}else{

				if(newuserFile != NULL){
					//printf("%s", user);
					fputs(user,newuserFile); //constitution du nouveau fichier sans l'utilisateur supprimé
				}
			}
			
		}
		fclose(userptr);
		fclose(newuserFile);
		free(user);

		int status = remove("./data/users.csv"); //effacement ancien fichier
 
		if (status == 0){
			printf("users.csv file deleted successfully.\n");
			writeToLogFile("INFO","old user file deleted");
		}else{
			writeToLogFile("ERREUR","can't delete old user file");
			printf("Unable to delete the file\n");
		}

		rename("./data/modUsers.csv","./data/users.csv"); // renommage ancien fichier
 
		
    }else{
        printf("ERREUR D OUVERTURE DE FICHIER");
		writeToLogFile("ERREUR", "Can't open users.csv at UserCreate");
        //ERREUR
    }
    writeToLogFile("INFO", "a user was modified");
    printf("Modifie des utisateurs\n");
	return 0;
}
